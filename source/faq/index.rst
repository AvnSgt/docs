##########################
Frequently Asked Questions
##########################

.. toctree::
  :maxdepth: 2

# How to enable system tray in polybar?

Open the file `~/.config/polybar/config` and edit the Openbox, i3 or bspwm:
`[bar/section]` accordingly. You only need to add `tray-position = right`. Then
you should restart polybar to see the changes (restart from CLI:
`pkill polybar; al-polybar-session`).

# How to enable autostart application in openbox?

Open the file `~/.config/openbox/autostart` and add any applications followed
with an ampersand (to send process to background). For example: `redshift &`
would add the eye-caring light filter application, `redshift`.

# How to auto-hide mouse cursor?

Install `unclutter` and add it to autostart like this:
`unclutter -b --timeout 3 --jitter 30 --exclude-root --ignore-scrolling &`