####
Rofi
####

----------------
Window Switcher:
----------------

.. image:: images/window_switcher.png
   :height: 239px
   :width: 798px
   :scale: 75
   :alt: Rofi Window Switcher
   :align: center

The window switcher shows the following bits of information in columns \(can be customized\):

1. Desktop name
2. Window class
3. Window title

Window mode features:

- closing applications with `Shift-Delete`
- custom command with `Shift-Return`

---------------------
Application Launcher:
---------------------

.. image:: images/application_launcher.png
   :height: 239px
   :width: 798px
   :scale: 75
   :alt: Rofi Application Launcher
   :align: center

The run mode allows users to quickly search for and launch a program.

Run mode features:

- `Shift-Return` to run the selected program in a terminal
- favorites list, with frequently used programs sorted on top
- custom entries, like aliases added by executing a command

----------------------------------
Desktop File Application Launcher:
----------------------------------

The desktop run mode allows users to quickly search and launch an application from the _freedesktop.org_ Desktop Entries. These are used by most Desktop Environments to populate launchers and menus. Drun mode features:

- favorites list, with frequently used programs sorted on top
- auto starting terminal applications in a terminal

-------------
SSH Launcher:
-------------

.. image:: images/ssh.png
   :height: 239px
   :width: 798px
   :scale: 75
   :alt: Rofi SSH Display
   :align: center

Quickly `ssh` into remote machines

- ## parses `~/.ssh/config` to find hosts

------------
Script Mode:
------------

Loads external scripts to add modes to **Rofi**, for example a file-browser.

``rofi  -show fb -modi fb:../Examples/rofi-file-browser.sh``


-----------
COMBI Mode:
-----------

Combine multiple modes in one view. This is especially useful when merging the window and run mode into one view. Allowing to quickly switch to an application, either by switching to it when it is already running or starting it.

Example to combine Desktop run and the window switcher:

``rofi -combi-modi window,drun -show combi -modi combi``


------------------
DMENU Replacement:
------------------

.. image:: images/dmenu.png
   :height: 239px
   :width: 798px
   :scale: 75
   :alt: Rofi DMENU
   :align: center

Drop in dmenu replacement. `(Screenshot shows rofi used by teiler) <https://github.com/carnager/teiler>`_.

**Rofi** features several improvements over dmenu to improve usability. There is the option to add an extra message bar \(`-mesg`\), pre-entering of text \(`-filter`\), or selecting entries based on a pattern \(`-select`\). Also highlighting \(`-u` and `-a`\) options and modi to force user to select one provided option \(`-only-match`\). In addition to this, rofi's dmenu mode can select multiple lines and write them to stdout.

------
Usage:
------

If used with `-show [mode]`, rofi will immediately open in the specified \[mode\]

If used with `-dmenu`, rofi will use data from STDIN to let the user select an option.

For example, to show a run dialog:

``rofi -show run``

To show a ssh dialog:

``rofi -show ssh``

------
DMENU:
------

If rofi is passed the `-dmenu` option, or run as `dmenu` \(ie, /usr/bin/dmenu is symlinked to /usr/bin/rofi\), it will use the data passed from STDIN.


| ~/scripts/my_script.sh | rofi -dmenu
| echo -e "Option #1\nOption #2\nOption #3" | rofi -dmenu


In both cases, rofi will output the user's selection to STDOUT.

-----------------------
Switching between MODI:
-----------------------

Type `Shift-/Left/Right` to switch between active modi.

-------------
Key Bindings:
-------------

+------------------------------+-------------------------------------------------------------------------------------------+
| Key                          | Action                                                                                    |
+==============================+===========================================================================================+
|                              |                                                                                           |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-v, Insert`             | Paste from clipboard                                                                      |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Shift-v, Shift-Insert` | Paste primary selection                                                                   |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-w`                     | Clear the line                                                                            |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-u`                     | Delete till the start of line                                                             |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-a`                     | Move to beginning of line                                                                 |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-e`                     | Move to end of line                                                                       |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-f, Right`              | Move forward one character                                                                |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Alt-f`                      | Move forward one word                                                                     |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-b, Left`               | Move back one character                                                                   |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Alt-b`                      | Move back one word                                                                        |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-d, Delete`             | Delete character                                                                          |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Alt-d`                 | Delete word                                                                               |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-h, Backspace`          | Backspace \(delete previous character\)                                                   |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Alt-h`                 | Delete previous word                                                                      |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-j,Ctrl-m,Enter`        | Accept entry                                                                              |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-n,Down`                | Select next entry                                                                         |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-p,Up`                  | Select previous entry                                                                     |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Page Up`                    | Go to the previous page                                                                   |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Page Down`                  | Go to the next page                                                                       |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Page Up`               | Go to the previous column                                                                 |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Page Down`             | Go to the next column                                                                     |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Enter`                 | Use entered text as a command \(in `ssh/run modi`\)                                       |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Shift-Enter`                | Launch the application in a terminal \(in run mode\)                                      |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Shift-Enter`                | Return the selected entry and move to the next item while keeping Rofi open. \(in dmenu\) |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Shift-Right`                | Switch to the next modi. The list can be customized with the -modi option.                |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Shift-Left`                 | Switch to the previous modi. The list can be customized with the -modi option.            |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Tab`                   | Switch to the next modi. The list can be customized with the -modi option.                |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-Shift-Tab`             | Switch to the previous modi. The list can be customized with the -modi option.            |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Ctrl-space`                 | Set selected item as input text.                                                          |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Shift-Del`                  | Delete entry from history.                                                                |
+------------------------------+-------------------------------------------------------------------------------------------+
| `grave`                      | Toggle case sensitivity.                                                                  |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Alt-grave`                  | Toggle levenshtein sort.                                                                  |
+------------------------------+-------------------------------------------------------------------------------------------+
| `Alt-Shift-S`                | Take a screenshot and store it in the Pictures directory.                                 |
+------------------------------+-------------------------------------------------------------------------------------------+

For the full list of key bindings, see: `rofi -show keys` or `rofi -help`.

--------------
Configuration:
--------------

There are currently three methods of setting configuration options:

- Local configuration. Normally, depending on XDG, in `~/.local/rofi/config`. This uses the Xresources format.
- Xresources: A method of storing key values in the Xserver. See `here <https://en.wikipedia.org/wiki/X_resources)>`_ for more information.
- Command line options: Arguments are passed to **Rofi**.

A distribution can ship defaults in `/etc/rofi.conf`.

The Xresources options and the command line options are aliased. To define option X set:

``rofi.X: value``

In the Xresources file. To set/override this from command line pass the same key prefixed with '-':

``rofi -X value``

To get a list of available options formatted as Xresources entries, run:

``rofi -dump-Xresources``

or in a more readable format:

``rofi -help``

The configuration system supports the following types:

- string
- integer \(signed and unsigned\)
- char
- Boolean

The Boolean option has a non-default command line syntax, to enable option X you do:

``rofi -X``

to disable it:

``rofi -no-X``

-------------
MANPAGE/WIKI:
-------------

For more detailed information, please see the `manpage <https://github.com/DaveDavenport/rofi/blob/next/doc/rofi.1.markdown>`_, the `wiki <https://github.com/DaveDavenport/rofi/wiki>`_, or the `forum <https://reddit.com/r/qtools/>`_.

-----------------
What is ROFI Not?
-----------------

Rofi is not:

- A preview application. In other words, it will not show a \(small\) preview of images, movies or other files.
- A UI toolkit.
- A library to be used in other applications.
- An application that can support every possible use-case. It tries to be generic enough to be usable by everybody. Specific functionality can be added using scripts.
- Just a dmenu replacement. The dmenu functionality is a nice 'extra' to **rofi**, not its main purpose.

----------------
Important Links:
----------------

`rofi <https://github.com/DaveDavenport/rofi>`_

`rofi wiki <https://github.com/DaveDavenport/rofi/wiki>`_
