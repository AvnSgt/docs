#####
Geany
#####


.. image:: images/geany_main.png
   :height: 808px
   :width: 942px
   :scale: 50
   :alt: Geany User Interface
   :align: center



**Geany** [1]_ is a text editor using the GTK+ toolkit with basic features of an integrated development environment. It was developed to provide a small and fast IDE, which has only a few dependencies from other packages. It supports many file types and has some nice features. For more details see `About <https://www.geany.org/Main/About>`_.

.. [1] : **Geany** is licensed under the terms of the GNU General Public License.
