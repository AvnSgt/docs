#####
Steam
#####


`Steam <http://store.steampowered.com/about/>`_ [1]_ is a popular game distribution platform by Valve.

Steam for Linux only supports Ubuntu 12.04 LTS and newer.`See here. <https://support.steampowered.com/kb_article.php?ref=1504-QHXN-8366>`_ Thus do not turn to Valve for support when running into issues with Steam on Arch Linux or its derivatives.

------------
Installation
------------

Enable the `Multilib <https://wiki.archlinux.org/index.php/Multilib>`_ repository and `install <https://wiki.archlinux.org/index.php/Install>`_ the `steam <https://www.archlinux.org/packages/?name=steam>`_ package.

The following fixes are needed to get Steam functioning properly on Arch Linux:

-  Install the 32-bit version of your `graphics driver <https://wiki.archlinux.org/index.php/Xorg#Driver_installation>`_ (the package in the
  OpenGL \(Multilib\) column\).
-  Make sure that you also have properly generated en\_US locales \ see
  `#Generating locales <https://wiki.archlinux.org/index.php/Locale#Generating_locales>`_,
   otherwise, the Steam client won't start with an invalid pointer error.
-  Steam makes heavy usage of the Arial font. A decent Arial font to use is
  `ttf-liberation <https://www.archlinux.org/packages/?name=ttf-liberation>`_
   or
  `the fonts provided by Steam <https://wiki.archlinux.org/index.php/Steam/Troubleshooting#Text_is_corrupt_or_missing>`_
  . Asian languages require
  `wqy-zenhei <https://www.archlinux.org/packages/?name=wqy-zenhei>`_
   to display properly.

- Information provided by: `Arch Linux wiki <https://wiki.archlinux.org/index.php/steam>`_ [1]_

.. [1] : Content is provided under the `GNU Free Documentation License 1.3 or later <https://www.gnu.org/copyleft/fdl.html>`_ unless otherwise noted.
