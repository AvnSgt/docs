#####
Urxvt
#####


**RXVT** [1]_ –our extended virtual terminal– is a terminal emulator for X11. It is a popular replacement for the standard ‘xterm’.


.. [1] : `RXVT <https://sourceforge.net/p/rxvt/code/HEAD/tree/branches/RXVT/doc/LSM.in>`_ is provided under the GNU Public License.
