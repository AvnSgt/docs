#####
Pacli
#####

.. image:: images/pacli-menu.png
   :height: 433px
   :width: 525px
   :scale: 100
   :alt: pacli interface
   :align: center

--------------
What is PACLI:
--------------

- A simple and interactive Bash Frontend for Pacman/Yaourt [1]_

- pacli is a CLI tool, which provides simple and advanced Pacman and Yaourt commands in an easy to use text interface.

--------------------------
Who is PACLI Intended For:
--------------------------

- pacli is meant for experienced/intermediate/advanced users, who have basic knowledge of the structure of their Linux system and how Pacman and Yaourt work.
- **Warning:** absolute beginners may be overwhelmed by the amount of choices pacli provides.

----
Why:
----

- pacli is included by default as it is a light weight, easy to use frontend for pacman/AUR.

.. [1] : Content is provided under `GPL-2.0 <https://github.com/Manjaro-Pek/pacli/blob/master/LICENSE>`_, unless otherwise noted.
